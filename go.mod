module gitlab.com/lightmeter/infrastructure/postfixmap

go 1.17

require (
	github.com/rs/zerolog v1.20.0
	gitlab.com/lightmeter/controlcenter v0.0.0-20220303214656-f07d5d55d674
)

require (
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
)
