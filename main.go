package main

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/lightmeter/controlcenter/util/emailutil"
	"gitlab.com/lightmeter/controlcenter/util/envutil"
	"net"
	"os"
	"regexp"
	"strings"
	"time"
)

var (
	routing = map[string]string{}

	commentRe = regexp.MustCompile(`^\s*#|^\s*$`)
	routeRe   = regexp.MustCompile(`^\s*(\S+)\s+(\S+)\s*#?`)
)

func loadRouting(filename string) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Bytes()
		if commentRe.Match(line) {
			log.Debug().Msgf("[routing file] Ignoring comment line '%s'", line)
			continue
		}

		m := routeRe.FindSubmatch(line)
		if m != nil {
			routing[string(m[1])] = string(m[2])
			log.Debug().Msgf("[routing file] Routing MX '%s' to relay '%s'", m[1], m[2])
			continue
		}

		log.Error().Msgf("[routing file] Line was not understood: '%s'", line)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal().Err(err).Msgf("Error reading routing file")
	}

	return nil
}

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339}).With().Logger()

	verbose, err := envutil.LookupEnvOrBool("MXMAP_VERBOSE", false, os.LookupEnv)
	if err != nil {
		log.Fatal().Err(err).Msgf("Wrong verbose value")
	}

	zerolog.SetGlobalLevel(func() zerolog.Level {
		if verbose {
			return zerolog.DebugLevel
		}

		return zerolog.InfoLevel
	}())

	routingFile := envutil.LookupEnvOrString("MXMAP_ROUTINGFILE", "not-set", os.LookupEnv)

	if err := loadRouting(routingFile); err != nil {
		log.Fatal().Err(err).Msg("Routing file not found")
	}

	listenTo := envutil.LookupEnvOrString("MXMAP_LISTEN", "not-set", os.LookupEnv)

	listen, err := net.Listen("tcp", listenTo)
	if err != nil {
		log.Fatal().Err(err).Msgf("Can't listen to '%s'", listenTo)
	}

	log.Info().Msgf("Listening to tcp connections on '%s'", listenTo)

	i := 0
	for {
		i++
		conn, err := listen.Accept()
		if err != nil {
			log.Fatal().Err(err).Msg("Could not accept connections")
		}

		go func(i int) {
			defer conn.Close()

			j := 0
			scanner := bufio.NewScanner(conn)
			for scanner.Scan() {
				j++
				query := scanner.Text()
				log.Debug().Msgf("[%d-%d] QUERY: '%s'", i, j, query)

				a := answer(query)
				log.Debug().Msgf("[%d-%d] ANSWR: '%s'", i, j, a)
				conn.Write([]byte(a))
			}
		}(i)
	}
}

// Postfix doc: http://www.postfix.org/tcp_table.5.html
func answer(query string) string {
	replyErr := func(query string, err error) string {
		log.Error().Err(err).Msgf("Error with original query '%s'\n", query)
		return fmt.Sprintf("400 %s", err)
	}

	q := strings.Split(query, " ")
	if q[0] != "get" {
		return replyErr(query, errors.New("First part of query is not 'get'"))
	}
	if len(q) != 2 {
		return replyErr(query, fmt.Errorf("Query includes more than two parts: %v", q))
	}

	domainOrEmail := q[1]
	if domainOrEmail == "*" {
		log.Debug().Msg("I know postfix is sending '*' sometimes but I don't know why")
		return "500 Don't know what '*' means…\n"
	}

	_, domain, _, err := emailutil.SplitPartial(domainOrEmail)
	if err != nil {
		return replyErr(query, errors.New("Couldn't be parsed into a domain"))
	}

	// Get MX for domain
	mxs, err := net.LookupMX(domain)
	if err != nil || len(mxs) == 0 {
		return "500 No MX\n"
	}

	mx := mxs[0].Host
	if mx[len(mx)-1] != '.' {
		return "500 Invalid MX\n"
	}

	mx = mx[0 : len(mx)-1]

	mx, err = emailutil.HostDomainFromDomain(mx)
	if err != nil {
		return replyErr(query, err)
	}

	log.Debug().Msgf("First MX record for domain '%s' is '%s'", domain, mx)

	route, exists := routing[mx]
	if !exists {
		return "500 Not Found\n"
	}

	return fmt.Sprintf("200 %v\n", route)
}
