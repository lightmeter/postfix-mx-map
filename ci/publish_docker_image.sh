#!/bin/sh

set -o pipefail
set -e
set -vx

if [ -z "${CI_COMMIT_TAG}" ]; then
	echo "ERROR: we publish docker images only on new tags for now" >&2
	return 1
fi

cat > /kaniko/.docker/config.json << EOF
{
  "auths": {
    "$CI_REGISTRY":{
      "username":"$CI_REGISTRY_USER",
      "password":"$CI_REGISTRY_PASSWORD"
    }
  }
}"
EOF

mkdir -p .docker-cache

/kaniko/executor \
  --context $CI_PROJECT_DIR \
  --dockerfile $CI_PROJECT_DIR/ci/Dockerfile \
  --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG \
  --destination $CI_REGISTRY_IMAGE:latest \
  --build-arg "GIT_TAG=$CI_COMMIT_TAG" \
  --cache-dir .docker-cache
