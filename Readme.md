

# Postfix MX map

## Config with environment variables

```bash
# Mandatory
export MXMAP_LISTEN='localhost:8091'         # the TCP address to listen on
export MXMAP_ROUTINGFILE='/path/to/routing'  # path to your routing file

# Optional
export MXMAP_VERBOSE='true'                  # to enable debug-level output (default: false)
```


## How-to use in postfix

This is just an example, adapt it to your needs.

```bash
postconf transport_maps="tcp:localhost:8091"
```


## Routing file

This is the file that defines your routing rules.
Here is an example:

```
# relay traffic to microsoft-hosted domains through 'relay1'
outlook.com    smtp:relay1.com:465

# relay traffic to google-hosted domains through 'relay2'
google.com     smtp:relay2.com:465
```
